﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OctoAwesome.Model.Blocks
{
    public interface IBlock
    {
        BoundingBox[] GetCollisionBoxes();
    }
}
